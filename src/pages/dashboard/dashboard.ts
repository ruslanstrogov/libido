import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ActFormPage } from '../act-form/act-form';

@IonicPage()
@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html',
})
export class DashboardPage {

  actFormPage: any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
      this.actFormPage = ActFormPage;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomeAdminPage');
  }

}
