import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ActFormPage } from './act-form';

@NgModule({
  declarations: [
      ActFormPage,
  ],
  imports: [
    IonicPageModule.forChild(ActFormPage),
  ],
})
export class ItemFormPageModule {}
