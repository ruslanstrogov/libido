import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

// Animations
import { AnimationService } from 'css-animator';

import { MyApp } from './app.component';

// Pages
import { DashboardPage } from '../pages/dashboard/dashboard';
import { ActFormPage } from '../pages/act-form/act-form';

import { HomePage } from '../pages-demo/home/home';
import { HomeAppPage } from '../pages-demo/home-app/home-app';
import { HomeBoxesPage } from '../pages-demo/home-boxes/home-boxes';
import { HomeBusinessPage } from '../pages-demo/home-business/home-business';
import { HomeTravelPage } from '../pages-demo/home-travel/home-travel';
import { HomeRestaurantPage } from '../pages-demo/home-restaurant/home-restaurant';
import { HomeAdminPage } from '../pages-demo/home-admin/home-admin';
import { HomeDarkPage } from '../pages-demo/home-dark/home-dark';
import { BlankPage } from '../pages-demo/blank/blank';
import { AboutPage } from '../pages-demo/about/about';
import { ElementsButtonsPage } from '../pages-demo/elements-buttons/elements-buttons';
import { ElementsGridPage } from '../pages-demo/elements-grid/elements-grid';
import { ElementsFormsPage } from '../pages-demo/elements-forms/elements-forms';
import { ElementsSlidersPage } from '../pages-demo/elements-sliders/elements-sliders';
import { ElementsTabsPage } from '../pages-demo/elements-tabs/elements-tabs';
import { ElementsChipsPage } from '../pages-demo/elements-chips/elements-chips';
import { ElementsFeaturesPage } from '../pages-demo/elements-features/elements-features';
import { ElementsTitlesPage } from '../pages-demo/elements-titles/elements-titles';
import { FunToastPage } from '../pages-demo/fun-toast/fun-toast';
import { FunActionPage } from '../pages-demo/fun-action/fun-action';
import { FunLoadingPage } from '../pages-demo/fun-loading/fun-loading';
import { FunGesturesPage } from '../pages-demo/fun-gestures/fun-gestures';
import { FunPopupsPage } from '../pages-demo/fun-popups/fun-popups';
import { FunSearchPage } from '../pages-demo/fun-search/fun-search';
import { BlocksCardsPage } from '../pages-demo/blocks-cards/blocks-cards';
import { BlocksTestimonialsPage } from '../pages-demo/blocks-testimonials/blocks-testimonials';
import { BlocksListsPage } from '../pages-demo/blocks-lists/blocks-lists';
import { BlogArticlesPage } from '../pages-demo/blog-articles/blog-articles';
import { BlogSingleArticlePage } from '../pages-demo/blog-single-article/blog-single-article';
import { ShopPage } from '../pages-demo/shop/shop';
import { ShopSingleProductPage } from '../pages-demo/shop-single-product/shop-single-product';
import { ContactPage } from '../pages-demo/contact/contact';
import { LoginPage } from '../pages-demo/login/login';
import { RegisterPage } from '../pages-demo/register/register';
import { LockPage } from '../pages-demo/lock/lock';
import { RtlPage } from '../pages-demo/rtl/rtl';
import { NativeRatePage } from '../pages-demo/native-rate/native-rate';
import { NativeFullscreenPage } from '../pages-demo/native-fullscreen/native-fullscreen';
import { NativeBadgePage } from '../pages-demo/native-badge/native-badge';
import { NativeLocalNotificationsPage } from '../pages-demo/native-local-notifications/native-local-notifications';
import { NativeSocialSharingPage } from '../pages-demo/native-social-sharing/native-social-sharing';
import { NativeCameraPreviewPage } from '../pages-demo/native-camera-preview/native-camera-preview';
import { NativeTtsPage } from '../pages-demo/native-tts/native-tts';

// Ionic stuff
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

// Providers
import { HttpModule } from '@angular/http';
import { WpProvider } from '../providers/wp/wp';
import { WcProvider } from '../providers/wc/wc';
import { UtilsProvider } from '../providers/utils/utils';
import { MomentModule } from 'angular2-moment';

// Native
import { AppRate } from '@ionic-native/app-rate';
import { AndroidFullScreen } from '@ionic-native/android-full-screen';
import { Badge } from '@ionic-native/badge';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { SocialSharing } from '@ionic-native/social-sharing';
import { CameraPreview, CameraPreviewPictureOptions, CameraPreviewOptions } from '@ionic-native/camera-preview';
import { TextToSpeech } from '@ionic-native/text-to-speech';

@NgModule({
  declarations: [
    MyApp,
    DashboardPage,
    ActFormPage,
    HomePage,
    HomeAppPage,
    HomeBoxesPage,
    HomeBusinessPage,
    HomeTravelPage,
    HomeRestaurantPage,
    HomeAdminPage,
    HomeDarkPage,
    BlankPage,
    AboutPage,
    ElementsButtonsPage,
    ElementsGridPage,
    ElementsFormsPage,
    ElementsSlidersPage,
    ElementsChipsPage,
    ElementsTitlesPage,
    ElementsFeaturesPage,
    FunToastPage,
    FunActionPage,
    FunLoadingPage,
    FunGesturesPage,
    FunPopupsPage,
    FunSearchPage,
    BlocksCardsPage,
    BlocksTestimonialsPage,
    BlocksListsPage,
    ElementsTabsPage,
    BlogArticlesPage,
    BlogSingleArticlePage,
    ShopPage,
    ShopSingleProductPage,
    ContactPage,
    LoginPage,
    RegisterPage,
    LockPage,
    RtlPage,
    NativeRatePage,
    NativeFullscreenPage,
    NativeBadgePage,
    NativeLocalNotificationsPage,
    NativeSocialSharingPage,
    NativeCameraPreviewPage,
    NativeTtsPage,
  ],
  imports: [
    BrowserModule,
    HttpModule,
    MomentModule,
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    DashboardPage,
    ActFormPage,
    HomePage,
    HomeAppPage,
    HomeBoxesPage,
    HomeBusinessPage,
    HomeTravelPage,
    HomeRestaurantPage,
    HomeAdminPage,
    HomeDarkPage,
    BlankPage,
    AboutPage,
    ElementsButtonsPage,
    ElementsGridPage,
    ElementsFormsPage,
    ElementsSlidersPage,
    ElementsChipsPage,
    ElementsTitlesPage,
    ElementsFeaturesPage,
    FunToastPage,
    FunActionPage,
    FunLoadingPage,
    FunGesturesPage,
    FunPopupsPage,
    FunSearchPage,
    BlocksCardsPage,
    BlocksTestimonialsPage,
    BlocksListsPage,
    ElementsTabsPage,
    BlogArticlesPage,
    BlogSingleArticlePage,
    ShopPage,
    ShopSingleProductPage,
    ContactPage,
    LoginPage,
    RegisterPage,
    LockPage,
    RtlPage,
    NativeRatePage,
    NativeFullscreenPage,
    NativeBadgePage,
    NativeLocalNotificationsPage,
    NativeSocialSharingPage,
    NativeCameraPreviewPage,
    NativeTtsPage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    AnimationService,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    WpProvider,
    WcProvider,
    UtilsProvider,
    AppRate,
    AndroidFullScreen,
    Badge,
    LocalNotifications,
    SocialSharing,
    CameraPreview,
    TextToSpeech,
  ]
})
export class AppModule {}
